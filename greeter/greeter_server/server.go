package main

import (
	greet "../greetpb"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

type Server struct {
	greet.UnimplementedGreetServiceServer
}


func (s *Server) GreetManyTimes(req *greet.PrimaryDecompositionRequest, stream greet.GreetService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was invoked with %v \n", req)


	res := PrimeFactors(req.RequestNum)

	for _,k := range res{
		println(k)
		res := &greet.PrimaryDecompositionResponse{ResponseNum: k}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
		time.Sleep(time.Second)
	}
	return nil
}

func PrimeFactors(number int32) []int32  {
	a := int(number)
	println(a)
	var rees []int32
	for i:= 2; i <= a; i++{
		println(i)
		if a%i == 0{
			if isPrime(i){
				for true{
					if a%i != 0 {
						break
					}
					rees = append(rees, int32(i))
					a=a/i
				}

			}
		}
	}
	return rees
}

func isPrime(a int) bool {
	flag := true
	for i := 2; i <= a/2; i++ {
		if a % i == 0 {
			flag = false
			break
		}
	}
	return flag
}



func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greet.RegisterGreetServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
